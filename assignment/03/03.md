# 0. Install applications
- install **`LaTeX`** at your system
- install **`git`** at your system
- install **`jupyter notebook`** at your system
- install **`visual studio code`** at your system

# 1. Share your github repository for the assignments with TA `[2pt]`
- create a new repository and name it **`assignment-machine-learning`** with **`Private`** mode
```
Repositories > New
```
- share the repository with the following teaching assistants
   - hong@cau.ac.kr 
   - hytae1993@hanmail.net
   - yuna@image.cau.ac.kr
   - kjhwanid@gmail.com
```
Repositories > assignment-machine-learning > Settings > Manage access > Invite a collaborator
```
- export the front page of the repository **`assignment-machine-learning`** as a PDF file with an arbitrary filename
```
Print the page at Repositories > assignment-machine-learning 
```
- submit the PDF file to the google classroom

# 2. Coding - compute the derivatives of image `[8pt]`
- download the notebook file [assignment_03_1.ipynb](https://gitlab.com/byungwoohong/class-machine-learning-2021-1/-/blob/master/assignment/03/assignment_03_1.ipynb)
- complete the blank in the codes so that the expected outputs are produced
- export the result as a PDF file with an arbitrary filename
- submit the PDF file to the google classroom

# 3. History of notebook [assignment_03_1.ipynb] at github `[2pt]`
- export the history page of notebook [assignment_03_1.ipynb] as a PDF file with an arbitrary filename
```
Repositories > assignment-machine-learning > assignment_03_1.ipynb > History
```
- submit the PDF file to the google classroom

# 4. Coding - compute Taylor approximate of function `[4pt]`
- download the notebook file [assignment_03_2.ipynb](https://gitlab.com/byungwoohong/class-machine-learning-2021-1/-/blob/master/assignment/03/assignment_03_2.ipynb)
- complete the blank in the codes so that the expected outputs are produced
- export the result as a PDF file with an arbitrary filename
- submit the PDF file to the google classroom

# 5. History of notebook [assignment_03_2.ipynb] at github `[2pt]`
- export the history page of notebook [assignment_03_2.ipynb] as a PDF file with an arbitrary filename
```
Repositories > assignment-machine-learning > assignment_03_2.ipynb > History
```
- submit the PDF file to the google classroom
